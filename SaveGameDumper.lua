local _load_cache_original = SavefileManager._load_cache
function SavefileManager:_load_cache(slot, ...)
    _load_cache_original(self, slot, ...)

    local meta_data = self:_meta_data(slot)
    local cache = meta_data.cache
    local is_setting_slot = slot == self.SETTING_SLOT

    if cache and not is_setting_slot then
        PrintTable(cache, 10)
    end
end
